type Node = {
	id: number,
	neighbors: number[],
};

type Graph = {
	nodes: Node[],
};


/**
 * Takes a "id_mc" (Minecraft ID number) and a list of ingredients and
 * returns the index of the ingredient in the list that has the given
 * "id_mc".
 */
function id_mc_to_ingredient_index(id_mc: number, ingredients): number {
	for(let i = 0; i < ingredients.length; i++)
	if(ingredients[i].id_mc == id_mc)
		return i;
	throw new ReferenceError("Non-existent id_mc " + id_mc);
}

/**
 * Takes an index `i` and a list of ingredients and returns the indexes of the
 * ingredients required to craft the `i`-th item in the ingredients list.
 *
 * Indexes are not repeated (i.e. the list is also a set).
 */
function get_ingredient_dependencies_indexes(i: number, ingredients): number[] {
	let dependencies = [];
	let ingredient = ingredients[i];
	if(!("recipe" in ingredient))
		return dependencies;
	for(let id_mc of ingredient.recipe) {
		// id_mc = 0 is a placeholder "air" ingredient, which is not a real
		// dependency.
		if(id_mc == 0)
			continue;
		let index = id_mc_to_ingredient_index(id_mc, ingredients);
		if(!dependencies.includes(index))
			dependencies.push(index);
	}
	return dependencies;
}



/**
 * Builds a Graph from a list of mincraft ingredients given by the
 * ingredients.js file.
 *
 * IDs are the ingredient indexes in the list.
 *
 * The returned graph is a Directed Acyclic Graph which contains the (i,j)-edge
 * if crafting the j-th ingredient requires the i-th ingredient.
 **/
function ingredients_to_graph(minecraft_ingredients): Graph {
	let ings = minecraft_ingredients;
	let nodes = [];
	// We skip the first item, it's "air".
	for(let i = 0; i < ings.length; i++) {
		let neighbors = get_ingredient_dependencies_indexes(
			i,
			minecraft_ingredients
		);
		let node = {
			id: i,
			neighbors: neighbors,
		};
		nodes.push(node);
	}
	let graph = {
		nodes: nodes
	};
	return graph;
}

/**
 * The "level" of a node captures it's "level" in a tech-tree. Items with no
 * dependencies have a level of 0, and items further along the tech-tree have
 * increasingly higher levels.
 *
 * Formally, for a Directed Acyclic Graph the level of each node is defined
 * as:
 *
 * level(i) = {
 *  0                                          if Neighbors(i) == {},
 *  max({level(j) | j in Neighbors(i) }) + 1   else
 *  }
 */
function get_node_level(i: number, graph: Graph): number {
	// TODO: This function could be memoized
	let node = graph.nodes[i];
	if(node.neighbors.length == 0)
		return 0;
	let levels = [];
	for(const j of node.neighbors) {
		levels.push(get_node_level(j, graph));
	}
	return Math.max(...levels)+1;
}

/**
 * Returns the list of "levels" of each node in the graph, in the same
 * order as they are listed in the graph.
 */
function get_node_levels(graph: Graph): number[] {
	let levels = [];
	for(let i = 0; i < graph.nodes.length; i++) {
		let level = get_node_level(i, graph);
		levels.push(level);
	}
	return levels;
}


/**
 * Return the indexes of the nodes in the reachable set of the given
 * node index. The list does not include the given index.
 *
 * It is assumed the graph is a Directed Acyclic Graph.
 */
function get_reachable_indexes(i: number, graph: Graph): number[] {
	let node = graph.nodes[i];
	if(node.neighbors.length == 0)
		return [];
	let reachable_indexes = [];
	for(const j of node.neighbors) {
		reachable_indexes.push(j);
		for(const k of get_reachable_indexes(j, graph))
			if(!reachable_indexes.includes(k))
				reachable_indexes.push(k);
	}
	return reachable_indexes;
}

export {
	ingredients_to_graph,
	get_node_level,
	get_node_levels,
	get_reachable_indexes,
};
